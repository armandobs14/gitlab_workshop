/**
 *  add
 * @param int x 
 * @param int y 
 */
let add = (x, y) => x + y;


/**
 * sub
 * @param int x 
 * @param int y 
 */
let sub = (x, y) => x - y;

// exporting functions
module.exports = {
    add,
    sub
}